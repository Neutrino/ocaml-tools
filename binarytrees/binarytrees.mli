type ('n, 'f) abs = Feuille of 'f | Noeud of 'n * ('n, 'f) abs * ('n, 'f) abs
type 'a abns = V | N of 'a * 'a abns * 'a abns
val hauteur_abs : ('a, 'b) abs -> int
val taille_abs : ('a, 'b) abs -> int
val dernier_abs : ('a, 'b) abs -> 'b
val hauteur_abns : 'a abns -> int
val taille_abns : 'a abns -> int
val dernier_abns : 'a abns -> 'a
val mmsq_abns : 'a abns -> 'b abns -> bool
val is_perfect_abs : ('a, 'b) abs -> bool
val is_sym_abns : 'a abns -> bool
val deepest_abns : 'a abns -> 'a
val aleatree_abns : int -> int abns
