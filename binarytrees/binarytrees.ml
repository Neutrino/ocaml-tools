(*
    DÉFINITION DES TYPES
    abs -> Arbre binaire stricte
    abns -> Arbre binaire non stricte
*)
type ('n, 'f) abs =
  |Feuille of 'f
  |Noeud of 'n * ('n,'f) abs * ('n, 'f) abs

type 'a abns =
  |V
  |N of 'a * 'a abns * 'a abns

(*HAUTEUR D'UN ARBRE abs*)
let rec hauteur_abs a =
  match a with
              |Feuille(_) -> 0
              |Noeud(_,c,d) -> 1 + max (hauteur_abs c) (hauteur_abs d)

(*TAILLE D'UN ARBRE abs*)
let rec taille_abs a =
  match a with
              | Noeud(k,b,c) -> 1 + taille_abs b + taille_abs c
              | Feuille(k) -> 1

(*DERNIÈRE FEUILLE D'UN ARBRE abs*)
let rec dernier_abs a =
  match a with
              | Noeud(_,_,c) -> dernier_abs c
              | Feuille(k) -> k

(*HAUTEUR D'UN ARBRE abns*)
let rec hauteur_abns a =
  match a with
              |V -> -1
              |N(_,g,d) -> 1 + max (hauteur_abns g) (hauteur_abns d)

(*TAILLE D'UN ARBRE abns*)
let rec taille_abns a =
  match a with
              | N(_,g,d) -> 1 + taille_abns g + taille_abns d
              | V -> 0

(*DERNIÈRE FEUILLE D'UN ARBRE abns*)
let rec dernier_abns a =
  match a with
              | N(e,_,V) -> e
              | N(_,_,d) -> dernier_abns d
              | V -> failwith("Emptree")

(* Même squelette *)
let rec mmsq_abns a1 a2 =
  match a1,a2 with
    | V,V -> true
    | N(_,l1,r1),N(_,l2,r2) -> mmsq_abns r1 r2 && mmsq_abns l1 l2
    | _ -> false

(* Vérifie si un arbre binaire strict est parfait *)
let is_perfect_abs a = taille_abs a = (let n = (hauteur_abs a +1) in (1 lsl n) -1)

(* Vérifie si  un arbre binaire non stricte est symétrique *)
let rec is_sym_abns a =
  match a with
  | V -> true
  | N(_,N(e1,g1,d1),N(e2,g2,d2)) -> e1=e2 && (is_sym_abns (N(e1,g1,d2))) && (is_sym_abns (N(e2,g2,d1)))
  | _ -> false

(* Cacule l'élément le plus profond d'un arbre binaire non stricte *)
let rec deepest_abns arbre =
  match arbre with
    | V -> failwith "erreur arbre vide"
    | N(e,V,V) -> e
    | N(e,d,g) -> if hauteur_abns d >= hauteur_abns g then deepest_abns d else deepest_abns g

(* Génère un arbre binaire non strict aléatoire *)
let aleatree_abns n =
  let rec aux m =
    if m=0 then V else
    let k = Random.int m in N(Random.int n,aux k,aux (m-1-k))
  in
  aux n