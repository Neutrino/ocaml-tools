# Quickexpo

A infix operator (`***`) and a function (`pow x n : int -> int -> int`) to calculate quickly (quick exponentiation algorithm) `x^n` where `x` and `n` are integers.

## Usage :
1. Infix operator : `x***n`
2. function : `pow x n`