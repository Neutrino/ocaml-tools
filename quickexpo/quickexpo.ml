(* Exponentiation rapide *)
let rec pow a n =
  if a = 2 then 1 lsl n else
    begin
      match n with
      | 0 -> 1
      | _ ->
      if n mod 2 = 0 then pow (a*a) (n/2) else a*(pow (a*a) (n/2))
    end
(* Avec opérateur infixe, utilisation x***n pour calculer x puissance n *)
let rec ( *** ) x n = pow x n
