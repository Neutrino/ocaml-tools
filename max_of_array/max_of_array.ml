let max_of_array ar =
  let maxi = ref 0 in
  let aux k = maxi:= max !maxi k in
  let () = Array.iter aux ar in
  !maxi