let rec split u =
  match u with
  | x::y::t -> let a,b = split t in x::a,y::b
  | x::[] -> [x],[]
  | [] -> [],[]

let rec merge u v compare =
  match u,v with
  | x::xs,y::ys -> if (compare x y < 0) then x::(merge xs v compare) else y::(merge u ys compare)
  | li,[] | [],li -> li

let rec merge_sort u compare =
  match u with
  | [] -> []
  | [x] -> [x]
  | _ -> let g,d = split u in merge (merge_sort g compare) (merge_sort d compare) compare