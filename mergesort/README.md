USER :
	- `merge_sort : 'a list -> ('a -> 'a -> int) -> 'a list`
	- merge_sort takes 2 arguments
		- list to sort
		- comparison function
			- `compare : 'a -> 'a -> int`
			- `compare x y` should return negative value if `x` has to be before `y`
DEV :
	- Look at `mergesort.mli` and `mergesort.ml` for further explanation
