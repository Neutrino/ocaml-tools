val split : 'a list -> 'a list * 'a list
val merge : 'a list -> 'a list -> ('a -> 'a -> int) -> 'a list
val merge_sort : 'a list -> ('a -> 'a -> int) -> 'a list
