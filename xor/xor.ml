let ( *:* ) a b =
  match a,b with
  | true,false | false, true -> true
  | _ -> false

let xor a b = a *:* b