let list_of_matrix m =
  let size = Array.length m in
  let lists = Array.make size [] in
  Array.iteri (fun i m' -> Array.iteri (fun k v -> if v then lists.(i)<-k::lists.(i)) m') m;
  lists
let matrix_of_list l =
  let size = Array.length l in
  let mat = Array.make_matrix size size false in
  Array.iteri (fun i li -> List.iter (fun k -> mat.(i).(k)<-true) li) l;
  mat