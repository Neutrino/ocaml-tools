(* GLOBAL MATRIX FUNCTIONS *)
let get_coeff m i j =
  if i>Array.length m || j>Array.length m.(0) then
    None
  else
  Some(m.(i).(j))
let set_coeff m i j x =
  if i>Array.length m || j>Array.length m.(0) then
    m
  else
    begin
      m.(i).(j)<-x;
      m
    end

(* INT MATRIX FUNCTIONS *)
let make_int_matrix m n = Array.make_matrix m n 0
let make_int_square_matrix n = Array.make_matrix n n 0
let int_identity n =
  let m = make_int_square_matrix n in
  for i=0 to n-1 do
    m.(i).(i)<-1
  done;
  m
let int_sum a b =
  let n_a = Array.length a in
  let n_b = Array.length b in
  let p_a = Array.length a in
  let p_b = Array.length b in
  if n_a <> n_b || p_a <> p_b then
    None
  else
    begin
      let m = make_int_matrix n_a p_a in
      for i = 0 to n_a-1 do
        for j = 0 to p_a-1 do
          m.(i).(j)<-a.(i).(j) + b.(i).(j)
        done;
      done;
      Some(m)
    end
let int_product a b =
  let n_b = Array.length b in
  let p_a = Array.length a.(0) in
  if n_b <> p_a then
    None
  else
    begin
      let n = n_b in
      let n_a = Array.length a in
      let p_b = Array.length b.(0) in
      let m = make_int_matrix n_a p_b in
      for i = 0 to n_a-1 do
        for j = 0 to p_b-1 do
          m.(i).(j)<-
          begin
            let s = ref 0 in
            for k = 0 to n-1 do
              s:=!s + a.(i).(k)*b.(k).(j)
            done;
            !s
          end
        done;
      done;
      Some(m)
    end
let fill_int_matrix m = 
  let acc = ref 0 in
  let n = Array.length m in
  let p = Array.length m.(0) in
  for i = 0 to n-1 do
    for j = 0 to p-1 do
      m.(i).(j)<- !acc;
      incr acc;
    done;
  done;
  m
let ( $*$ ) a b = int_product a b
let ( $+$ ) a b = int_sum a b

(* FLOAT MATRIX FUNCTIONS *)
let make_float_matrix m n = Array.make_matrix m n 0.
let make_float_square_matrix n = Array.make_matrix n n 0.
let float_identity n =
  let m = make_float_square_matrix n in
  for i=0 to n-1 do
    m.(i).(i)<-1.
  done;
  m
let float_sum a b =
  let n_a = Array.length a in
  let n_b = Array.length b in
  let p_a = Array.length a in
  let p_b = Array.length b in
  if n_a <> n_b || p_a <> p_b then
    None
  else
    begin
      let m = make_float_matrix n_a p_a in
      for i = 0 to n_a-1 do
        for j = 0 to p_a-1 do
          m.(i).(j)<-a.(i).(j) +. b.(i).(j)
        done;
      done;
      Some(m)
    end
let float_product a b =
  let n_b = Array.length b in
  let p_a = Array.length a.(0) in
  if n_b <> p_a then
    None
  else
    begin
      let n = n_b in
      let n_a = Array.length a in
      let p_b = Array.length b.(0) in
      let m = make_float_matrix n_a p_b in
      for i = 0 to n_a-1 do
        for j = 0 to p_b-1 do
          m.(i).(j)<-
          begin
            let s = ref 0. in
            for k = 0 to n-1 do
              s:= !s +. a.(i).(k) *. b.(k).(j)
            done;
            !s
          end
        done;
      done;
      Some(m)
    end
let fill_float_matrix m = 
  let acc = ref 0. in
  let n = Array.length m in
  let p = Array.length m.(0) in
  for i = 0 to n-1 do
    for j = 0 to p-1 do
      m.(i).(j)<- !acc;
      acc:= !acc +. 1.;
    done;
  done;
  m
let ( $*$. ) a b = float_product a b
let ( $+$. ) a b = float_sum a b

(* TYPE CASTING *)

let int_matrix_of_float_matrix m =
  let n = Array.length m in
  let p = Array.length m.(0) in
  let m' = make_int_matrix n p in
  for i = 0 to n do
    for j = 0 to p do
      m'.(i).(j)<-int_of_float m.(i).(j)
    done;
  done;
  m'

let float_matrix_of_int_matrix m =
  let n = Array.length m in
  let p = Array.length m.(0) in
  let m' = make_float_matrix n p in
  for i = 0 to n do
    for j = 0 to p do
      m'.(i).(j)<- float_of_int m.(i).(j)
    done;
  done;
  m'