val get_coeff : 'a array array -> int -> int -> 'a option
val set_coeff : 'a array array -> int -> int -> 'a -> 'a array array
val make_int_matrix : int -> int -> int array array
val make_int_square_matrix : int -> int array array
val int_identity : int -> int array array
val int_sum : int array array -> int array array -> int array array option
val int_product :
  int array array -> int array array -> int array array option
val fill_int_matrix : int array array -> int array array
val ( $*$ ) : int array array -> int array array -> int array array option
val ( $+$ ) : int array array -> int array array -> int array array option
val make_float_matrix : int -> int -> float array array
val make_float_square_matrix : int -> float array array
val float_identity : int -> float array array
val float_sum :
  float array array -> float array array -> float array array option
val float_product :
  float array array -> float array array -> float array array option
val fill_float_matrix : float array array -> float array array
val ( $*$. ) :
  float array array -> float array array -> float array array option
val ( $+$. ) :
  float array array -> float array array -> float array array option
val int_matrix_of_float_matrix : float array array -> int array array
val float_matrix_of_int_matrix : int array array -> float array array
