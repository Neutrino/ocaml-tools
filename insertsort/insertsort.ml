let rec insere u x compare =
  match u with
  | h::t when compare x h < 1 -> x::u
  | h::t -> h::(insere t x compare)
  | [] -> [x]

let insert_sort u compare =
  let rec aux u acc =
    match u with
    | h::t -> aux t (insere acc h compare)
    | [] -> acc
  in aux u []