(* LEFT CHILD RIGHT SIBLING *)

type 'a anb = Nn of 'a * 'a anb list
type 'a abns =
  | V
  | N of 'a * 'a abns * 'a abns

let to_bin arbre =
  let rec aux node sib =
    match node,sib with
      | Nn(e,[]),[] -> N(e,V,V)
      | Nn(e,[]),f::fs -> N(e,V,aux f fs)
      | Nn(e,a::xs),[] -> N(e,aux a xs, V)
      | Nn(e,a::xs),f::fs -> N(e,aux a xs, aux f fs)
  in
  aux arbre []

let to_anb arbre =
  let rec aux node =
    match node with
      | V -> failwith "Arbre vide"
      | N(e,V,V) -> let tone = Nn(e,[]) in tone,[tone]
      | N(e,kids,V) -> let tone = Nn(e,snd(aux kids)) in tone,[tone]
      | N(e,V,sib) -> let tone = Nn(e,[]) in tone,tone::snd(aux sib)
      | N(e,kids,sib) -> let tone = Nn(e,snd(aux kids)) in tone,tone::snd(aux sib)
  in fst(aux arbre)