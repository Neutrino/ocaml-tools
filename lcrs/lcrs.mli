type 'a anb = Nn of 'a * 'a anb list
type 'a abns = V | N of 'a * 'a abns * 'a abns
val to_bin : 'a anb -> 'a abns
val to_anb : 'a abns -> 'a anb
